package net;

import "core:mem"
import "core:fmt"

wsa_init :: proc()
{
    data: WSAData;
    err := wsa_startup(MAKEWORD(2, 2), &data);
    assert(err == 0);
}

wsa_clean :: proc()
{
    wsa_cleanup();
}

tcp_data :: struct {
    socket : SOCKET,
}

tcp_connect :: proc(pipe : ^Pipe, host : cstring, port : cstring) -> i32
{
    addrinfo: ^Addrinfo;
    hints: Addrinfo;
    hints.ai_family   = .INET;
    hints.ai_socktype = .STREAM;
    hints.ai_protocol = .TCP;

    if err := get_addrinfo(host, port, &hints, &addrinfo); err != 0  do return err;
    defer free_addrinfo(addrinfo);

    socket := socket(addrinfo.ai_family, addrinfo.ai_socktype, addrinfo.ai_protocol);
    if socket == INVALID_SOCKET  do return wsa_get_last_error();

    error := connect(socket, addrinfo.ai_addr, cast(i32)addrinfo.ai_addrlen);
    if error == SOCKET_ERROR do return wsa_get_last_error();

    data := cast(^tcp_data)pipe.data;
    data.socket = socket;

    return 0;
}

tcp_listen :: proc(pipe : ^Pipe, host : cstring, port : cstring) -> i32
{
    data := cast(^tcp_data)pipe.data;

    addrinfo: ^Addrinfo;
    hints: Addrinfo;
    hints.ai_family   = .INET;
    hints.ai_socktype = .STREAM;
    hints.ai_protocol = .TCP;

    if err := get_addrinfo(host, port, &hints, &addrinfo); err != 0  do return err;
    defer free_addrinfo(addrinfo);

    socket := socket(addrinfo.ai_family, addrinfo.ai_socktype, addrinfo.ai_protocol);
    if socket == INVALID_SOCKET  do return wsa_get_last_error();
    data.socket = socket;

    bind_error := bind(socket, addrinfo.ai_addr, i32(addrinfo.ai_addrlen));
    if bind_error == SOCKET_ERROR do return wsa_get_last_error();

    listen_error := listen(socket, 32);
    if listen_error == SOCKET_ERROR do return wsa_get_last_error();
    return 0;
}

tcp_accept :: proc(pipe : ^Pipe) -> (i32, Pipe)
{
    data := cast(^tcp_data)pipe.data;

    client := accept(data.socket, nil, nil);
    if client == INVALID_SOCKET do return wsa_get_last_error(), Pipe{};

    client_pipe : Pipe = default_tcp_pipe();
    (cast(^tcp_data)client_pipe.data).socket = client;

    return 0, client_pipe;
}

tcp_shutdown :: proc(pipe : ^Pipe) -> i32
{
    data := cast(^tcp_data)pipe.data;
    error := shutdown(data.socket, SD_SEND);
    if error == SOCKET_ERROR do return wsa_get_last_error();

    error = close_socket(data.socket);
    if error == SOCKET_ERROR do return wsa_get_last_error();
    return 0;
}

tcp_send :: proc(pipe : ^Pipe, buffer : []u8) -> i32
{
    data := cast(^tcp_data)pipe.data;
    send_result := send(data.socket, cast(string)buffer, 0);
    if send_result == SOCKET_ERROR {
        fmt.println("Send error: ", wsa_get_last_error());
        return -1;
    }
    return send_result;
}

tcp_recv :: proc(pipe : ^Pipe, buffer : []u8) -> i32
{
    data := cast(^tcp_data)pipe.data;
    recv_result := recv(data.socket, cast(^u8)&buffer[0], i32(len(buffer)), 0);
    if recv_result == SOCKET_ERROR {
        fmt.println("Recv error: ", wsa_get_last_error());
        return -1;
    }
    return recv_result;
}

default_tcp_pipe :: proc() -> Pipe
{
    output : Pipe;
    wsa_init();
    output.data = cast(^u8)mem.alloc(size_of(tcp_data));
    output.connect = tcp_connect;
    output.listen = tcp_listen;
    output.accept = tcp_accept;
    output.shutdown = tcp_shutdown;
    output.send = tcp_send;
    output.recv = tcp_recv;
    return output;
}