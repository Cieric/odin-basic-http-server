package net;

Pipe :: struct
{
    connect : proc(pipe : ^Pipe, host : cstring, port : cstring) -> i32,
    listen : proc(pipe : ^Pipe, host : cstring, port : cstring) -> i32,
    accept : proc(pipe : ^Pipe) -> (i32, Pipe),
    shutdown : proc(pipe : ^Pipe) -> i32,
    send : proc(pipe : ^Pipe, data : []u8) -> i32,
    recv : proc(pipe : ^Pipe, data : []u8) -> i32,
    data : ^u8,
};