package main

import net "net"
import "core:fmt"
import "core:strings"

send_string :: proc(pipe : ^net.Pipe, str : string)
{
    buf: [1024]byte;
    fmt.bprintf(buf[:], str);
    send_result := pipe.send(pipe, buf[0:len(str)]);
    if send_result < 0 {
        fmt.println("Send error: ", send_result);
    }
}

main :: proc()
{
    data: net.WSAData;
    err := net.wsa_startup(net.MAKEWORD(2, 2), &data);
    assert(err == 0);
    defer net.wsa_cleanup();

    pipe : net.Pipe = net.default_tcp_pipe();
    
    pipe.listen(&pipe, "127.0.0.1", "80");

    @static recv_buffer: [1024]byte;
    for {
        fmt.println("Listening...");
        accept_err, client := pipe.accept(&pipe);
        defer client.shutdown(&client);
        if accept_err != 0 do continue;

        fmt.println("Client Accepted...");

        //wait for request.
        receive_result := client.recv(&client, recv_buffer[:]);

        fmt.println(receive_result, cast(string)(recv_buffer[:receive_result]));

        //send response
        builder := strings.make_builder();

        strings.write_string(&builder, "HTTP/1.0 200 OK\r\n");
        strings.write_string(&builder, "Server: shwa server/0.1.0\r\n");
        strings.write_string(&builder, "Content-Type: text/html\r\n");
        strings.write_string(&builder, "\r\n");
        strings.write_string(&builder, "<HTML><TITLE>WOW IT WORKS</TITLE></HTML>\r\n");
        strings.write_string(&builder, "<BODY>ODIN HTTP SERVER BABYYYY</BODY>\r\n");

        send_string(&client, strings.to_string(builder));
    }
}